export const getRandomNumber = (minV, maxV) => {
  const min = Math.ceil(minV)
  const max = Math.floor(maxV)
  return Math.floor(Math.random() * (max - min)) + min
}

export const getRandomDate = (start, end) => new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()))
