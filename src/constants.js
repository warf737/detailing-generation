import { getRandomNumber } from '@/utils'

export const SERVICES = [{ type: 'out', name: 'Исходящий звонок' }, { type: 'in', name: 'Входящий звонок' }]
export const NUMBERS = {
  generated: [
    // рандомные сгенерированные номера
    '79207403853',
    '79206196225',
    '79205666209',
    '79208363120',
    '79201824557',
    // рандомные сгенерированные номера
    '79511935434',
    '79518928364',
    '79514803112',
    '79512736144',
    '79514778119'
  ],
  relatives: [
    // сгенерированные номера родственников
    '79203293916',
    '79205166978',
    '79081644571',
    '79205176881', // искомый номер родственника SIC!
    '79519312325',
    '79082354567',
    '79101236431',
    '79089876543'
  ],
  belrus: [
    // сгенерированные номера беларуси
    '375292043960',
    '375292731693',
    '375292059642',
    '375292861914',
    '375292993946',
    '375292076717'
  ]
}

export const REQUIRED_CALLS = [
  // входящий на ДР Максима
  {
    type: 'input',
    type_name: 'Входящий звонок',
    date: new Date(2019, 9, 21),
    value: getRandomNumber(10, 65)
  },
  {
    type: 'input',
    type_name: 'Входящий звонок',
    date: new Date(2020, 9, 21),
    value: getRandomNumber(10, 65)
  },
  {
    type: 'input',
    type_name: 'Входящий звонок',
    date: new Date(2021, 9, 21),
    value: getRandomNumber(10, 65)
  },
  // исходящий на ДР
  {
    type: 'output',
    type_name: 'Исходящий звонок',
    date: new Date(2019, 0, 17),
    value: getRandomNumber(10, 65)
  },
  {
    type: 'output',
    type_name: 'Исходящий звонок',
    date: new Date(2020, 0, 17),
    value: getRandomNumber(10, 65)
  },
  {
    type: 'output',
    type_name: 'Исходящий звонок',
    date: new Date(2021, 0, 17),
    value: getRandomNumber(10, 65)
  },
  {
    type: 'output',
    type_name: 'Исходящий звонок',
    date: new Date(2022, 0, 17),
    value: 0
  },
  // исходящий на НГ
  {
    type: 'output',
    type_name: 'Исходящий звонок',
    date: new Date(2019, 11, 31),
    value: getRandomNumber(10, 65)
  },
  {
    type: 'output',
    type_name: 'Исходящий звонок',
    date: new Date(2020, 11, 31),
    value: getRandomNumber(10, 65)
  },
  {
    type: 'output',
    type_name: 'Исходящий звонок',
    date: new Date(2021, 11, 31),
    value: 0
  }
]
